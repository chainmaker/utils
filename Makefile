VERSION=v2.3.6

gomod:
	go get chainmaker.org/chainmaker/common/v2@${VERSION}
	go get chainmaker.org/chainmaker/pb-go/v2@${VERSION}
	go get chainmaker.org/chainmaker/protocol/v2@v2.3.7
	go mod tidy

lint:
	golangci-lint run ./...